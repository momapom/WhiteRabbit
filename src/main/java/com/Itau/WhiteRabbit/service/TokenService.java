package com.Itau.WhiteRabbit.service;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class TokenService {
	String secret = "WriteRabbit";
	
	public String gerar(String i) {
		try {
//			Date dataExpirar = new Date();
//			Calendar calendario = Calendar.getInstance();

//			calendario.setTime(dataExpirar);
//			calendario.add(Calendar.MINUTE, 30);

//			dataExpirar = calendario.getTime();

			Algorithm algorithm = Algorithm.HMAC256(secret);
//			return JWT.create().withExpiresAt(dataExpirar).withClaim("funcional", i).sign(algorithm);
			return JWT.create().withClaim("funcional", i).sign(algorithm);
		} catch (Exception exception){
			exception.printStackTrace();
			return null;
		}
	}

	public String verificar(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(secret);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("funcional").asString();
		} catch (Exception exception){
			exception.printStackTrace();

			return null;
		}
	}
}
