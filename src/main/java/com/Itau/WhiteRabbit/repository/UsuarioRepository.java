package com.Itau.WhiteRabbit.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.Itau.WhiteRabbit.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String>{
	Optional<Usuario> findByEmail(String email);
}
