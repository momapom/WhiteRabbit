package com.Itau.WhiteRabbit.model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Atividade {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAtividade;
	private String nomeAtividade;
	private String detalheAtividade;
	private String tipoAtividade;
	private String codigoProjeto;
	private int data;
	
	private LocalTime horaInicio;
	private LocalTime horaFim;
//	private int horaInicio;
//	private int horaFim;
	@ManyToOne(optional=false)
	private Usuario usuario;
	public int getIdAtividade() {
		return idAtividade;
	}
	public void setIdAtividade(int idAtividade) {
		this.idAtividade = idAtividade;
	}
	public String getNomeAtividade() {
		return nomeAtividade;
	}
	public void setNomeAtividade(String nomeAtividade) {
		this.nomeAtividade = nomeAtividade;
	}
	public String getDetalheAtividade() {
		return detalheAtividade;
	}
	public void setDetalheAtividade(String detalheAtividade) {
		this.detalheAtividade = detalheAtividade;
	}
	public String getTipoAtividade() {
		return tipoAtividade;
	}
	public void setTipoAtividade(String tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}
	public String getCodigoProjeto() {
		return codigoProjeto;
	}
	public void setCodigoProjeto(String codigoProjeto) {
		this.codigoProjeto = codigoProjeto;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public LocalTime getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}
	public LocalTime getHoraFim() {
		return horaFim;
	}
	public void setHoraFim(LocalTime horaFim) {
		this.horaFim = horaFim;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getHoraCalculada() {
		Duration duration = Duration.between(this.getHoraInicio(), this.getHoraFim());
		long totalMinutes = duration.toMinutes();
		long minutes = totalMinutes % 60;
		

		return String.format("%02d:%02d", duration.toHours(), minutes);
	}
}